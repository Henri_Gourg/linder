FROM ubuntu

RUN apt-get update
RUN apt -y install curl wget
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt -y install nodejs

RUN mkdir -p /usr/share/linder

COPY . /usr/share/linder

ENV MONGODB_URI="mongodb://mongodb:27017/linder_api"
ENV TOKEN_SECRET="09f26e402586e2faa8da4c98a35f1b20d6b033c6097befa8be3486a829587fe2f90a832bd3ff9d42710a4da095a2ce285b009f0c3730cd9b8e1af3eb84df6611API=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"

EXPOSE 3333

CMD ["node", "/usr/share/linder/dist/apps/linder/main.js"]