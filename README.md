# Linder

Linder project for Ynov Architecture course


## Description
Linder is a swipe-based application that connects candidates and companies for job offers. 
Recruiters can also help companies find their ideal future employees.

## Documentation
Route access : /api-docs

## Installation
First you have to create .env and .test.env files

```
npm i
```

## Usage
```
npm start
```
or
```
docker-compose up --build
```

## Roadmap


## Authors and acknowledgment
Created by 

   - Michaela ALFOLDIOVA
   - Armelle BENGOCHEA
   - Henri GOURGUE
   - Tyler ESCOLANO
   - Nicolas MARRY

