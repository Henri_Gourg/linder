import {
  AccountInterface,
} from '../models/interfaces';
import { Account } from '../models';

import { Query, Document } from 'mongoose';

export default class AccountRepository {

  public async getAccountByEmail(email: string): Promise<Query<any, Document<AccountInterface>>> {
    return await Account.findOne({ email: email });
  }

  public async getAccountById(id: string): Promise<Query<any, Document<AccountInterface>>> {
    return await Account.findOne({ id: id });
  }

}
