import { JobberInterface } from '../models/interfaces';
import { Jobber, Account } from '../models';

import { Query, Document } from 'mongoose';

export default class JobberRepository {
  public async saveJobber(jobber: JobberInterface): Promise<JobberInterface> {
    const account = await new Account({ ...jobber.account }).save();
    return await new Jobber({ ...jobber, accountId: account.id }).save();
  }

  public async getJobber(jobberId: string): Promise<Query<any, Document<JobberInterface>>> {
    return await Jobber.findOne({ id: jobberId }).populate('account');
  }

  public async getJobbers(): Promise<Query<any, Document<JobberInterface>>> {
    return await Jobber.find({});
  }

  public async getJobberByAccount(accountId: string): Promise<Query<any, Document<JobberInterface>>> {
    return await Jobber.findOne({ accountId: accountId }).populate('account');
  }
}
