import { JobAnnouncementInterface } from '../models/interfaces';
import { JobAnnouncement, Match } from '../models';

import { Query, Document } from 'mongoose';

export default class JobAnnouncementRepository {
  public async saveJobAnnouncement(jobAnnouncement: JobAnnouncementInterface): Promise<JobAnnouncementInterface> {
    return await new JobAnnouncement({ ...jobAnnouncement }).save();
  }

  public async getJobAnnouncement(jobAnnouncementId: string): Promise<Query<any, Document<JobAnnouncementInterface>>> {
    return await JobAnnouncement.findOne({ id: jobAnnouncementId });
  }
  public async getJobAnnouncements(jobberId: string): Promise<Query<any, Document<JobAnnouncementInterface[]>>> {
    const matchIds = await Match.find({ jobberId }, { job_announcementId: 1 });
    return await JobAnnouncement.find({ status: 'ouvert', id: { $nin: matchIds } }, { status: 0 }).populate({
      path: 'company',
      options: { select: { name: 1, logo: 1, description: 1 } },
    });
  }
}
