import { CompanyInterface } from '../models/interfaces';
import { Company, Account } from '../models';

import { Query, Document } from 'mongoose';

export default class CompanyRepository {
  public async saveCompany(company: CompanyInterface): Promise<CompanyInterface> {
    const account = await new Account({ ...company.account }).save();
    return await new Company({ ...company, accountId: account.id }).save();
  }

  public async getCompany(companyId: string): Promise<Query<any, Document<CompanyInterface>>> {
    return await Company.findOne({ id: companyId }).populate('account');
  }

  public async getBySiren(siren: number): Promise<Query<any, Document<CompanyInterface>>> {
    return await Company.findOne({ siren });
  }

  public async getCompanyByAccount(accountId: string): Promise<Query<any, Document<CompanyInterface>>> {
    return await Company.findOne({ accountId: accountId }).populate('account');
  }
}
