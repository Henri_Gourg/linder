import { MatchInterface } from '../models/interfaces';
import { Match } from '../models';

import { Query, Document } from 'mongoose';

export default class MatchRepository {
  public async saveMatch(data: any): Promise<MatchInterface> {
    return await new Match({ ...data }).save();
  }

  public async getMatch(matchId: string): Promise<Query<any, Document<MatchInterface>>> {
    return await Match.findOne({ id: matchId });
  }

  public async getMatchsForAnnouncement(announcementId: string): Promise<Query<any, Document<MatchInterface>>> {
    return await Match.findOne({ job_announcementId: announcementId }).populate('announcement');
  }

  public async getMatchsForAnnouncementByJobber(
    announcementId: string,
    jobberId: string
  ): Promise<Query<any, Document<MatchInterface>>> {
    return await Match.findOne({ job_announcementId: announcementId, jobberId: jobberId });
  }

  public async updateMatch(data: any): Promise<Query<any, Document<MatchInterface>>> {
    const match = await Match.findOne({ id: data.match.id });
    const bool = data.company_match ? { company_match: data.company_match } : { jobber_match: data.jobber_match };

    Object.assign(match, {
      ...bool,
    });

    await match.save();

    return match;
  }

  public async getAnnouncementMatchFull(id: string): Promise<Query<any, Document<MatchInterface>>> {
    const matchs = await Match.find({ job_announcementId: id, company_match: true, jobber_match: true }).populate(
      'jobber'
    );

    return matchs;
  }

  public async getAnnouncementMatchFullForJobber(id: string): Promise<Query<any, Document<MatchInterface>>> {
    const matchs = await Match.find({ jobberId: id, company_match: true, jobber_match: true }).populate({
      path: 'announcement',
      populate: {
        path: 'company',
        select: ['name', 'logo', 'description'],
      },
    });

    return matchs;
  }
}
