import mongoose from "mongoose";

import { CompanyInterface } from './interfaces';

const Company = new mongoose.Schema<CompanyInterface>({

  id: {
    type: String,
    required: true,
  },

  accountId: {
    type: String,
    required: true
  },

  siren: {
    type: Number,
    required: true
  },

  name: {
    type: String,
    required: true,
  },

  logo: {
    type: String,
    required: true,
  },

  localisation: {
    type: String,
    required: true,
  },

  description: {
    type: String,
    required: true,
  },

  site: {
    type: String,
    required: true,
  },
  
  created_at: {
    type: Date,
    default: Date.now,
  },

});

Company.virtual('account',{
  ref: 'Account',
  localField: 'accountId',
  foreignField: 'id',
  justOne: true
});

Company.set('toObject', { virtuals: true });
Company.set('toJSON', { virtuals: true });


export default mongoose.model('Company', Company);