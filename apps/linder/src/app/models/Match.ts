import mongoose from "mongoose";

import { MatchInterface } from './interfaces';

const Match = new mongoose.Schema<MatchInterface>({

  id: {
    type: String,
    required: true,
  },

  jobberId: {
    type: String,
    required: true,
  },

  job_announcementId: {
    type: String,
    required: true,
  },

  jobber_match: {
    type: Boolean,
  },

  company_match: {
    type: Boolean,
  },

  created_at: {
    type: Date,
    default: Date.now,
  },
});

Match.virtual('jobber',{
  ref: 'Jobber',
  localField: 'jobberId',
  foreignField: 'id',
  justOne: true
});

Match.virtual('announcement',{
  ref: 'JobAnnouncement',
  localField: 'job_announcementId',
  foreignField: 'id',
  justOne: true
});

Match.set('toObject', { virtuals: true });
Match.set('toJSON', { virtuals: true });

export default mongoose.model('Match', Match);
