import mongoose from "mongoose";

import { JobAnnouncementInterface } from './interfaces';

const JobAnnouncement = new mongoose.Schema<JobAnnouncementInterface>({

  id: {
    type: String,
    required: true,
  },
  companyId: {
    type: String,
    required: true,
  },
  job_name: {
    type: String,
    required: true,
  },
  recruiter_allowed: {
    type: Boolean,
    required: true,
  },
  contract_type: {
    type: String,
    enum: ["CDI","CDD"],
    required: true,
  },
  localisation: {
    type: String,
    required: true,
  },
  job_start: {
    type: Date,
    required: true,
  },
  job_description: {
    type: String,
    required: true,
  },
  profile: {
    type: String,
    required: false,
  },
  salary: [{
    type: Number,
    enum: [[15000, 20000], [20000, 25000], [25000, 30000], [30000, 35000], [35000, 40000], [40000,45000], [45000, 55000], [55000, 65000]],
    required: true,
  }],
  keywords: [String],
  created_at: {
    type: Date,
    default: Date.now,
  },
  status: {
    type: String,
    enum: ["ouvert","pourvue","fermé"],
    required: true,
  }

});
JobAnnouncement.virtual('company', {
  ref: 'Company',
  localField: 'companyId',
  foreignField: 'id',
  justOne: true
});

JobAnnouncement.set('toObject', { virtuals: true });
JobAnnouncement.set('toJSON', { virtuals: true });

export default mongoose.model('JobAnnouncement', JobAnnouncement);
