import mongoose from "mongoose";

import { JobberInterface } from './interfaces';

const Jobber = new mongoose.Schema<JobberInterface>({

  id: {
    type: String,
    required: true,
  },

  accountId: {
    type: String,
    required: true,
  },

  firstname: {
    type: String,
    required: true,
  },

  lastname: {
    type: String,
    required: true,
  },

  photo: {
    type: String,
    required: false,
  },

  keywords: [String],

  description: {
    type: String,
    required: false,
  },

  salary: [{
    type: Array,
    enum: [[15000, 20000], [20000, 25000], [25000, 30000], [30000, 35000], [35000, 40000], [40000, 45000], [45000, 55000], [55000, 65000]],
    required: false,
  }],

  contract_type: [{
    type: String,
    required: false,
  }],

  last_connected_at: {
    type: Date,
    required: false,
  },

  created_at: {
    type: Date,
    default: Date.now,
  },

});

Jobber.virtual('account',{
  ref: 'Account',
  localField: 'accountId',
  foreignField: 'id',
  justOne: true
});

Jobber.set('toObject', { virtuals: true });
Jobber.set('toJSON', { virtuals: true });

export default mongoose.model('Jobber', Jobber);