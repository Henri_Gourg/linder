import mongoose from "mongoose";

import { AccountInterface } from './interfaces';

const Account = new mongoose.Schema<AccountInterface>({

  id: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
  },

  password: {
    type: String,
    required: true,
  },

  type: {
    type: String,
    required: true,
  },

  notification: {
    type: Boolean,
    required: false,
  },

});

export default mongoose.model('Account', Account);