import mongoose from "mongoose";

import { RecruiterInterface } from './interfaces';

const Recruiter = new mongoose.Schema<RecruiterInterface>({

  id: {
    type: String,
    required: true,
  },

  accountId: {
    type: String,
    required: true,
  },

  firstname: {
    type: String,
    required: true,
  },

  lastname: {
    type: String,
    required: true,
  },

});

export default mongoose.model('Recruiter', Recruiter);