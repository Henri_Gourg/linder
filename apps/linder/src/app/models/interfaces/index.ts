export interface CompanyInterface {
  id: string;
  account: AccountInterface;
  siren: number;
  name: string;
  logo: string;
  localisation: string;
  description: string;
  site: string;
}

export interface CompanyToAdd {
  account: AccountToAdd;
  siren: number;
  name: string;
  logo: string;
  localisation: string;
  description: string;
  site: string;
}

export interface AccountInterface {
  id: string;
  email: string;
  password: string;
  type: string;
}

export interface AccountToAdd {
  email: string;
  password: string;
  type: string;
}

export interface JobberInterface {
  id: string;
  account: AccountInterface;
  firstname: string;
  lastname: string;
  photo: string;
  keywords: string[];
  description: string;
  salary: number[];
  contract_type: string[];
  last_connected_at: Date;
}

export interface JobberProfile {
  id: string;
  name: string;
  photo: string;
  keywords: string[];
  description: string;
}

export interface JobberToAdd {
  account: AccountToAdd;
  firstname: string;
  lastname: string;
  photo: string;
  keywords: string[];
  description: string;
  salary: number[];
  contract_type: string[];
}

export interface RecruiterInterface {
  id: string;
  accountId: string;
  firstname: string;
  lastname: string;
}

export interface MatchInterface {
  id: string;
  jobberId: string;
  job_announcementId: string;
  jobber_match: boolean;
  company_match: boolean;
}

export interface JobAnnouncementInterface {
  id: string;
  companyId: string;
  job_name: string;
  recruiter_allowed: boolean;
  contract_type: string;
  localisation: string;
  job_start: Date;
  job_description: string;
  profile: string;
  salary: number[];
  keywords: string[];
  status: string;
}

export interface JobAnnouncementToAdd {
  companyId: string;
  job_name: string;
  recruiter_allowed: boolean;
  contract_type: string;
  localisation: string;
  job_start: Date;
  job_description: string;
  profile: string;
  salary: number[];
  keywords: string[];
  status: string;
}

export interface MessageResponse {
  message: string;
}

export interface LoginResponse {}
