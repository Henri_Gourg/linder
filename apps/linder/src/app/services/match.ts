import { v4 as uuidv4 } from 'uuid';

import { MatchRepository, JobberRepository, CompanyRepository, JobAnnouncementRepository } from '../repository';
import { BadRequest, NotFound } from '../../utils/errors';
import { MatchInterface } from '../models/interfaces';

const matchRepository = new MatchRepository();
const jobberRepository = new JobberRepository();
const companyRepository = new CompanyRepository();
const announcementRepository = new JobAnnouncementRepository();

export const createMatch = async (
  job_announcementId: string,
  user: string,
  boolean: boolean,
  jobber?: string
): Promise<MatchInterface> => {
  let match = await matchRepository.getMatchsForAnnouncement(job_announcementId);
  const announcement = await announcementRepository.getJobAnnouncement(job_announcementId);
  const isJobber = await jobberRepository.getJobber(user);
  const isCompany = await companyRepository.getCompany(user);

  if (!announcement) {
    throw BadRequest('announcement_not_exists');
  }

  if (announcement?.status !== 'ouvert') {
    throw BadRequest('announcement_not_active');
  }

  if (isCompany) {
    if (announcement.companyId !== isCompany.id) {
      throw BadRequest('Not allowed');
    }

    if (match && match?.company_match) {
      throw BadRequest('already_match');
    }

    if (match) {
      match = await matchRepository.updateMatch({ match, company_match: boolean });
    } else {
      match = await matchRepository.saveMatch({
        id: uuidv4(),
        job_announcementId: job_announcementId,
        jobberId: jobber,
        company_match: boolean,
      });
    }
  }

  if (isJobber) {
    if (match && match?.jobber_match) {
      throw BadRequest('already_match');
    }

    if (match) {
      match = await matchRepository.updateMatch({ match, jobber_match: boolean });
    } else {
      match = await matchRepository.saveMatch({
        id: uuidv4(),
        job_announcementId: job_announcementId,
        jobberId: user,
        jobber_match: boolean,
      });
    }
  }

  return match;
};

export const getAnnouncementMatches = async (announcementId: string, companyId: string): Promise<any[]> => {
  const announcement = await announcementRepository.getJobAnnouncement(announcementId);
  const company = await companyRepository.getCompany(companyId);

  if (!announcement || announcement.status != 'ouvert' || announcement.companyId !== company.id) {
    throw BadRequest('Not allowed');
  }

  const matchs = await matchRepository.getAnnouncementMatchFull(announcementId);

  const res = {
    offre: announcement,
    ...matchs.map((e) => ({
      name: e.jobber.firstname + ' ' + e.jobber.lastname.charAt(0),
      photo: e.jobber.photo,
      description: e.jobber.description,
      keywords: e.jobber.keywords,
    })),
  };

  return res;
};

export const getJobberMatches = async (jobberId: string): Promise<any[]> => {
  const matchs = await matchRepository.getAnnouncementMatchFullForJobber(jobberId);

  return matchs;
};
