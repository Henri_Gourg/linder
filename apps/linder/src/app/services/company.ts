import { v4 as uuidv4 } from 'uuid';

import { CompanyRepository, AccountRepository, JobAnnouncementRepository } from '../repository';
import { BadRequest, NotFound } from '../../utils/errors';
import {
  CompanyInterface,
  CompanyToAdd,
  JobAnnouncementInterface,
} from '../models/interfaces';

const companyRepository = new CompanyRepository();
const accountRepository = new AccountRepository();
const jobAnnouncementRepository = new JobAnnouncementRepository();

export const createCompany = async (companyDTO: CompanyToAdd): Promise<CompanyInterface> => {

  const exists = await accountRepository.getAccountByEmail(companyDTO.account.email);

  if (exists) {
    throw BadRequest('already_exists');
  }

  const existsBySiren = await companyRepository.getBySiren(companyDTO.siren);

  if (existsBySiren) {
    throw BadRequest('already_exists');
  }

  const company = await companyRepository.saveCompany({
    ...companyDTO,
    id: uuidv4(),
    account: {
        ...companyDTO.account,
        id: uuidv4(),
        type: 'company',
    },
});

    return company;
};

export const getCompany = async (id: string): Promise<CompanyInterface> => {

    const company = await companyRepository.getCompany(id);

    if(!company) {
      throw NotFound('company_not_found');
    }

    return company;
};