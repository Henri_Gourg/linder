import { v4 as uuidv4 } from 'uuid';

import { JobberRepository, AccountRepository } from '../repository';
import { BadRequest, NotFound} from '../../utils/errors';
import {
  JobberToAdd,
  JobberInterface,
} from '../models/interfaces';

const jobberRepository = new JobberRepository();
const accountRepository = new AccountRepository();

export const createJobber = async (jobberDTO: JobberToAdd): Promise<JobberInterface> => {

  const exists = await accountRepository.getAccountByEmail(jobberDTO.account.email);

  if (exists) {
    throw BadRequest('already_exists');
  }

  const jobber = await jobberRepository.saveJobber({
    ...jobberDTO,
    id: uuidv4(),
    last_connected_at: new Date(),
    account: {
      ...jobberDTO.account,
      id: uuidv4(),
      type: 'jobber',
    },
  });

  return jobber;
};

export const getJobber = async (id: string): Promise<JobberInterface> => {

  const jobber = await jobberRepository.getJobber(id);

  if(!jobber) {
    throw NotFound('jobber_not_found');
  }

  return jobber;
};
