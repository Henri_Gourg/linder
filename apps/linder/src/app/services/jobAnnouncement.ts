import { v4 as uuidv4 } from 'uuid';

import { JobAnnouncementRepository, JobberRepository, CompanyRepository, MatchRepository } from '../repository';
import { BadRequest, NotFound } from '../../utils/errors';
import { JobAnnouncementToAdd, JobAnnouncementInterface, JobberProfile, CompanyInterface } from '../models/interfaces';

const jobAnnouncementRepository = new JobAnnouncementRepository();
const jobberRepository = new JobberRepository();
const companyRepository = new CompanyRepository();
const matchRepository = new MatchRepository();

function intersect_arrays(a, b) {
  let sorted_a = a;
  let sorted_b = b;
  let common = [];
  let a_i = 0;
  let b_i = 0;

  while (a_i < a.length && b_i < b.length) {
    if (sorted_a[a_i] === sorted_b[b_i]) {
      common.push(sorted_a[a_i]);
      a_i++;
      b_i++;
    } else if (sorted_a[a_i] < sorted_b[b_i]) {
      a_i++;
    } else {
      b_i++;
    }
  }
  return common.length;
}

export const createJobAnnouncement = async (
  announcementDTO: JobAnnouncementToAdd
): Promise<JobAnnouncementInterface> => {
  const announcement = await jobAnnouncementRepository.saveJobAnnouncement({
    ...announcementDTO,
    id: uuidv4(),
    job_start: new Date(announcementDTO.job_start),
  });

  return announcement;
};

export const getJobAnnouncement = async (id: string): Promise<JobAnnouncementInterface> => {
  const announcement = await jobAnnouncementRepository.getJobAnnouncement(id);

  if (!announcement) {
    throw NotFound('job_announcement_not_found');
  }

  return announcement;
};

const isValidAnnouncement = (company: CompanyInterface, annoucement: JobAnnouncementInterface): boolean => {
  if (!annoucement) {
    return false;
  }

  if (annoucement.companyId !== company.id) {
    return false;
  }

  return true;
};

export const getJobAnnouncementProfilesList = async (companyID: string, announcementID: string): Promise<any> => {
  const company = await companyRepository.getCompany(companyID);
  const announcement = await jobAnnouncementRepository.getJobAnnouncement(announcementID);
  const jobbers = await jobberRepository.getJobbers();
  const matchs = await matchRepository.getMatchsForAnnouncement(announcementID);

  if (!announcement || announcement.status != 'ouvert') {
    throw NotFound('job_announcement_not_found');
  }

  if (!isValidAnnouncement(company, announcement)) {
    throw NotFound('announcement_not_valid');
  }

  let list = [];
  const profilesList = [];

  jobbers.forEach((jobber) => {
    const currDate = new Date();
    const diffDate = currDate.getTime() - jobber.last_connected_at.getTime();
    const days = 14 * 24 * 60 * 60 * 100;

    if (diffDate <= days) {
      const numberOfKeyMatches = intersect_arrays(announcement.keywords, jobber.keywords);
      list.push({ nb: numberOfKeyMatches, jobber });
    }
  });

  list.sort((a, b) => parseFloat(a.nb) + parseFloat(b.nb));
  list = list.slice(0, 10);

  list.forEach((jobber) => {
    profilesList.push({
      name: jobber.jobber.firstname + ' ' + jobber.jobber.lastname.charAt(0),
      photo: jobber.jobber.photo,
      description: jobber.jobber.description,
      keywords: jobber.jobber.keywords,
    });
  });

  return profilesList;
};

export const getJobAnnouncements = async (jobberId: string): Promise<JobAnnouncementInterface[]> => {
  const announcements = await jobAnnouncementRepository.getJobAnnouncements(jobberId);

  if (!announcements) {
    throw NotFound('job_announcement_not_found');
  }

  return announcements;
};
