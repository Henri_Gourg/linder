import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { environment } from '../../environments/environment';

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {

    if (!req.headers["authorization"] && !req.headers["auth"]) {
      return res.status(401).send("No token sent");
    }

    let token = <string>req.headers["authorization"] || <string>req.headers["auth"];
    let jwtPayload;
    token = token.replace('Bearer ','');
    
    try {

        jwtPayload = <any>jwt.verify(token, environment.TOKEN_SECRET);
        
        res.locals.jwtPayload = jwtPayload;

    } catch(e) {
        return res.status(401).send("Token invalid");
    }

    const { id, email, type } = jwtPayload;

    const newToken = jwt.sign(
        { id: id, email: email, type: type},
        environment.TOKEN_SECRET,
        { expiresIn: "1h" }
    );

    res.setHeader("token", newToken);

    next();

};
