import { Request, Response, NextFunction } from "express";
import AccountRepository from "../repository/account-repository";

const accountRepository: AccountRepository = new AccountRepository;

export const checkRole = (roles: Array<string>) => {

    return async (req: Request, res: Response, next: NextFunction) => {

        const accountId = res.locals.jwtPayload.accountId;

        try {

            const user = await accountRepository.getAccountById(accountId);

            if (roles.indexOf(user.type) > -1) next();
            else res.status(401).send("Access not allowed");

        } catch(e) {

            res.status(401).send("Something went wrong while checking role");
        }
    }
};
