import { Tags, Body, Controller, Get, Post, Path, Route, Security, SuccessResponse, Res, TsoaResponse } from 'tsoa';

import { CompanyToAdd } from '../models/interfaces';
import * as companyService from '../services/company';
import { uuidV4Validate } from '../../utils/validate';
import { CompanyInterface, MessageResponse } from '../models/interfaces';
@Route('/api/companies')
@Tags('Company')
@Security('jwt')
export class CompanyController extends Controller {
  /**
   * GET A COMPANY
   */
  @Get('{id}')
  @SuccessResponse(200, 'Success')
  public async get(
    @Path() id: string,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<CompanyInterface> {
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await companyService.getCompany(id);
  }

  /**
   * USER CASE n°1 - CRÉATION D’UNE ENTREPRISE
   */
  @Post()
  @SuccessResponse(201, 'Created')
  public async post(@Body() requestBody: CompanyToAdd): Promise<{ id: string }> {
    const { id } = await companyService.createCompany(requestBody);
    this.setStatus(201);
    return Promise.resolve({ id });
  }
}
