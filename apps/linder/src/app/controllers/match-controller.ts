import {
  Tags,
  Controller,
  Get,
  Put,
  Path,
  Route,
  Security,
  SuccessResponse,
  Res,
  Body,
  Request,
  TsoaResponse,
} from 'tsoa';

import * as matchService from '../services/match';
import { uuidV4Validate } from '../../utils/validate';
import { MessageResponse, MatchInterface } from '../models/interfaces';
import { getProfile } from '../../utils/jwt';
@Route('/api/job-annoucements/matches')
@Tags('Match')
@Security('jwt')
export class MatchController extends Controller {
  /**
   * USER CASE n°5 - MATCH SUR UN PROFIL
   */
  @Put('{id}/match/{boolean}')
  @Security('jwt')
  @SuccessResponse(200, 'Success')
  public async udpateMatchForAnnouncement(
    @Path() id: string,
    @Path() boolean: string,
    @Request() request: any,
    @Body() requestBody: any,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<MatchInterface> {
    const token = request.headers['auth'];
    const user = await getProfile(token);
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await matchService.createMatch(id, user, boolean === 'true', requestBody.jobber);
  }

  /**
   * USER CASE n°7 - MATCH SUR UNE OFFRE
   */
  /*
  @Put('{id}/match/{boolean}')
  @Security('jwt')
  @SuccessResponse(200, 'Success')
  public async postMatchForAnnouncementByJobber(
    @Path() id: string,
    @Path() boolean: string,
    @Request() request: any,
    @Body() requestBody: any,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<MatchInterface> {
    const token = request.headers['auth'];
    const user = await getProfile(token);
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await matchService.createMatch(id, user, boolean === 'true', requestBody.jobber);
  }
  */
  /**
   * USER CASE n°8 - MATCH ENTREPRISE
   */
  @Get('{id}/match')
  @Security('jwt')
  @SuccessResponse(200, 'Success')
  public async getAnnouncementMatches(
    @Path() id: string,
    @Request() request: any,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<any[]> {
    const token = request.headers['auth'];
    const company = await getProfile(token);
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await matchService.getAnnouncementMatches(id, company);
  }
}
