import { Tags, Body, Controller, Post, Route, SuccessResponse } from 'tsoa';
import * as jwt from 'jsonwebtoken';
import { MessageResponse, LoginResponse } from '../models/interfaces';
import { environment } from '../../environments/environment';
import { AccountRepository, JobberRepository, CompanyRepository } from '../repository';
//const accountRepository: AccountRepository = new AccountRepository();

@Route('/api/login')
@Tags('Auth')
export class AuthController extends Controller {
  /**
   * Ajouter une description ici (facultative)
   */
  @Post()
  @SuccessResponse(201, 'Created')
  public async post(
    @Body() requestBody: { email: string; password: string }
  ): Promise<MessageResponse | LoginResponse> {
    try {
      const accountRepository: AccountRepository = new AccountRepository();
      const jobberRepository: JobberRepository = new JobberRepository();
      const companyRepository: CompanyRepository = new CompanyRepository();

      const user = await accountRepository.getAccountByEmail(requestBody.email);
      user['accountId'] = user.id;

      switch (user.type) {
        case 'jobber':
          const jobber = await jobberRepository.getJobberByAccount(user.id);
          user.id = jobber.id;
          break;

        case 'company':
          const company = await companyRepository.getCompanyByAccount(user.id);
          user.id = company.id;
          break;

        default:
          break;
      }

      if (!user) {
        this.setStatus(403);
        return { message: 'Bad login or password' };
      } else {
        if (!(user.password == requestBody.password)) {
          this.setStatus(403);
          return { message: 'Bad login or password' };
        }

        const token = jwt.sign({ id: user.id, email: user.username, type: user.type }, environment.TOKEN_SECRET, {
          expiresIn: '1h',
        });
        return { token };
      }
    } catch (e) {
      this.setStatus(401);
    }
  }
}
