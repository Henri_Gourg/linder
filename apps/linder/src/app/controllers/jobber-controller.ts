import { Tags, Body, Controller, Get, Post, Path, Route, Security, SuccessResponse, Res, TsoaResponse } from 'tsoa';

import * as jobberService from '../services/jobber';
import * as matchService from '../services/match';
import { uuidV4Validate } from '../../utils/validate';
import { JobberToAdd, JobberInterface, MessageResponse } from '../models/interfaces';
@Route('/api/jobbers')
@Tags('Jobber')
export class JobController extends Controller {
  /**
   * GET A JOBBER
   */
  @Get('{id}')
  @Security('jwt', ['company', 'jobber'])
  @SuccessResponse(200, 'Success')
  public async get(
    @Path() id: string,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<JobberInterface> {
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await jobberService.getJobber(id);
  }

  /**
   * USER CASE n°3 - CRÉATION D’UN PROFIL
   */
  @Post()
  @SuccessResponse(201, 'Created')
  public async post(@Body() requestBody: JobberToAdd): Promise<{ id: string }> {
    const { id } = await jobberService.createJobber(requestBody);
    this.setStatus(201);
    return { id };
  }

  /**
   * USER STORY n° 9 - MATCH CANDIDATE
   */
  @Get('{id}/match')
  @Security('jwt')
  @SuccessResponse(200, 'Success')
  public async getJobberMatches(@Path() id: string): Promise<any[]> {
    this.setStatus(200);
    return await matchService.getJobberMatches(id);
  }
}
