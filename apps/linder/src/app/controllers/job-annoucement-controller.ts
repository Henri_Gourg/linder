import {
  Tags,
  Body,
  Controller,
  Get,
  Post,
  Path,
  Security,
  Route,
  SuccessResponse,
  Res,
  TsoaResponse,
  Request,
} from 'tsoa';

import * as jobAnnouncementService from '../services/jobAnnouncement';
import { uuidV4Validate } from '../../utils/validate';
import { getProfile } from '../../utils/jwt';
import { MessageResponse, JobAnnouncementInterface, JobAnnouncementToAdd } from '../models/interfaces';

@Route('/api/job-annoucements')
@Tags('Anoucement')
export class JobAnnoucementController extends Controller {
  /**
   * Ajouter une description ici (facultative)
   */
  @Get('{id}')
  @Security('jwt', ['company', 'jobber'])
  @SuccessResponse(200, 'Success')
  public async get(
    @Path() id: string,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<JobAnnouncementInterface> {
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await jobAnnouncementService.getJobAnnouncement(id);
  }

  /**
   * USER CASE n°4 - LISTE DES PROFILS
   */
  @Get('{id}/ProfileList')
  @Security('jwt', ['company'])
  @SuccessResponse(200, 'Success')
  public async getProfileList(
    @Path() id: string,
    @Request() request: any,
    @Res() malformedId: TsoaResponse<400, MessageResponse>
  ): Promise<JobAnnouncementInterface> {
    const token = request.headers['auth'];
    const companyProfile = await getProfile(token);
    if (!uuidV4Validate(id)) {
      return malformedId(400, { message: 'Malformed Id' });
    }
    this.setStatus(200);
    return await jobAnnouncementService.getJobAnnouncementProfilesList(companyProfile.id, id);
  }

  /**
   * USER CASE n°6 - LISTE DES OFFRES
   */
  @Get()
  @Security('jwt', ['jobber'])
  @SuccessResponse(200, 'Success')
  public async getAll(@Request() request: any): Promise<JobAnnouncementInterface[]> {
    const token = request.headers['auth'];
    const user = await getProfile(token);
    this.setStatus(200);
    return await jobAnnouncementService.getJobAnnouncements(user.id);
  }

  /**
   * USER CASE n°2 - CRÉATION D’UNE OFFRE
   */
  @Post()
  @Security('jwt', ['company', 'jobber'])
  @SuccessResponse(201, 'Created')
  public async post(@Body() requestBody: JobAnnouncementToAdd): Promise<{ id: string }> {
    const { id } = await jobAnnouncementService.createJobAnnouncement(requestBody);
    this.setStatus(201);
    return { id };
  }
}
