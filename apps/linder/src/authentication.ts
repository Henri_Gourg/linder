import * as express from 'express';
import * as jwt from 'jsonwebtoken';

import { environment } from './environments/environment';

export const expressAuthentication = async (
  request: express.Request,
  securityName: string,
  scopes?: string[]
): Promise<any> => {
  let token = <string>request.headers['auth'];
  token.replace('Bearer ', '');
  let jwtPayload;
  if (securityName === 'jwt') {
    return await new Promise((resolve, reject) => {
      if (!token) {
        reject(new Error('NO VALID TOKEN'));
      }
      try {
        jwtPayload = <any>jwt.verify(token, environment.TOKEN_SECRET);
        const { id, email, type } = jwtPayload;
        const newToken = jwt.sign({ id: id, email: email, type: type }, environment.TOKEN_SECRET, { expiresIn: '1h' });
        resolve(newToken);
      } catch (e) {
        reject(new Error('NO VALID TOKEN'));
      }

      /*
      jwt.verify(token, environment.TOKEN_SECRET, function (err: any, decoded: any) {
        if (err) {
          reject(err);
        } else {
          for (let scope of scopes) {
            if (!decoded.scopes.includes(scope)) {
              reject(new Error('JWT does not contain required scope.'));
            }
          }

          resolve(decoded);
        }
      });*/
    });
  }
};
