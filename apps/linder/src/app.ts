import express, { Response as ExResponse, Request as ExRequest, NextFunction } from 'express';
import { ValidateError } from 'tsoa';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import swaggerUi from 'swagger-ui-express';
import helmet from 'helmet';
import cors from 'cors';
import './app/controllers';
import { RegisterRoutes } from './routes';

export const app = express();

app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('tiny'));
app.use(express.static('public'));

app.get('/api', (req: ExRequest, res: ExResponse) => {
  res.send({ message: 'Welcome to linder!' });
});

app.use(
  '/api/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerUrl: `http://localhost:${process.env.port || 3330}/api`,
    swaggerOptions: {
      url: '/swagger.json',
    },
  })
);

RegisterRoutes(app);

app.use(function errorHandler(err: unknown, req: ExRequest, res: ExResponse, next: NextFunction): ExResponse | void {
  if (err instanceof ValidateError) {
    console.warn(`Caught Validation Error for ${req.path}:`, err.fields);
    return res.status(422).json({
      message: 'Validation Failed',
      details: err?.fields,
    });
  }
  if (err instanceof Error) {
    if (err.message === 'NO VALID TOKEN') {
      return res.status(422).json({
        message: 'No authorized',
      });
    }
    if (err.message === 'TOKEN EXPIRED') {
      return res.status(422).json({
        message: 'Token expired',
      });
    }
    return res.status(500).json({
      message: 'Internal Server Error',
    });
  }

  next();
});

app.use(function notFoundHandler(_req, res: ExResponse) {
  res.status(404).send({
    message: 'Not Found',
  });
});
